;;; mermaid-ts-mode.el --- A mode for editing MermaidJS files based on tree-sitter -*- lexical-binding: t; -*-

;; Copyright 2023 Alex Figl-Brick

;; Author: Alex Figl-Brick <alex@alexbrick.me>
;; Version: 0.1
;; Package-Requires: ((emacs "29.1"))
;; URL: https://gitlab.com/bricka/emacs-mermaid-ts-mode

;; This program is free software: you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see
;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This package uses the `treesit' functionality added in Emacs 29 to
;; provide a nice mode for editing MermaidJS.

;;; Code:

(require 'treesit)
(require 'ob)

(defvar mermaid-ts-mode-indent-offset 4)

(defvar mermaid-ts-mode-syntax-table
  (let ((st (make-syntax-table)))
    (modify-syntax-entry ?% ". 12" st)
    st))

(defvar mermaid-ts-mode-mmdc-command "mmdc"
  "The command to run to generate a diagram.")

(defvar mermaid-ts-mode-input-file (concat (temporary-file-directory) "mermaid-ts-mode-input.mmd")
  "The file to write input to for compiling.")

(defvar mermaid-ts-mode-diagram-file (concat (temporary-file-directory) "mermaid-ts-mode-output.png")
  "The file to write Mermaid diagrams to.")

(defvar mermaid-ts-mode--compile-process nil
  "Internal variable storing the current compile process.")

(defun mermaid-ts-mode--compilation-sentinel (_process event)
  "Handle EVENT for PROCESS."
  (when (string= event "finished\n")
    (display-buffer (find-file-noselect mermaid-ts-mode-diagram-file t))))

(defun mermaid-ts-mode-compile-buffer ()
  "Compile the current buffer to a diagram and display it."
  (interactive)
  (when mermaid-ts-mode--compile-process
    (delete-process mermaid-ts-mode--compile-process)
    (setq mermaid-ts-mode--compile-process nil))
  (write-region nil nil mermaid-ts-mode-input-file)
  (setq mermaid-ts-mode--compile-process
        (start-process "mmdc" "mmdc" mermaid-ts-mode-mmdc-command "-i" mermaid-ts-mode-input-file "-o" mermaid-ts-mode-diagram-file))
  (set-process-sentinel mermaid-ts-mode--compile-process #'mermaid-ts-mode--compilation-sentinel))

(defvar mermaid-ts-mode--treesit-settings
  (when (treesit-available-p)
    (treesit-font-lock-rules
     :language 'mermaid
     :feature 'keyword
     '(["activate"
        "actor"
        "and"
        "as"
        "deactivate"
        "end"
        "erdiagram"
        "flowchart"
        "left of"
        "loop"
        "note "
        "over"
        "par"
        "participant"
        "right of"
        "sequenceDiagram"
        "subgraph"
        ] @font-lock-keyword-face)

     :language 'mermaid
     :feature 'comment
     '((comment) @font-lock-comment-face)

     :language 'mermaid
     :feature 'string
     '((er_role) @font-lock-string-face
       (sequence_text) @font-lock-string-face
       (flow_text_literal) @font-lock-string-face
       (flow_arrow_text) @font-lock-string-face
       (flow_text_quoted) @font-lock-string-face)

     :language 'mermaid
     :feature 'type
     '((er_attribute_type) @font-lock-type-face)

     :language 'mermaid
     :feature 'variable
     '((er_entity_name) @font-lock-variable-name-face
       (sequence_actor) @font-lock-variable-name-face
       (sequence_alias) @font-lock-variable-name-face
       (er_attribute_name) @font-lock-variable-name-face
       (flow_vertex_id) @font-lock-variable-name-face)

     :language 'mermaid
     :feature 'constant
     '((er_attribute_key_type_pk) @font-lock-constant-face
       (er_attribute_key_type_fk) @font-lock-constant-face
       (flowchart_direction_tb) @font-lock-constant-face
       (flowchart_direction_bt) @font-lock-constant-face
       (flowchart_direction_rl) @font-lock-constant-face
       (flowchart_direction_lr) @font-lock-constant-face))))

(defvar mermaid-ts-mode--treesit-indent-rules
  (let ((offset mermaid-ts-mode-indent-offset))
    `((mermaid
       ((node-is "}") parent-bol 0)
       ((node-is "end") parent-bol 0)
       ((parent-is "diagram_flow") parent-bol ,offset)
       ((parent-is "diagram_er") parent-bol ,offset)
       ((parent-is "er_stmt_entity_block") parent-bol ,offset)
       ((parent-is "er_stmt_entity_block_inner") parent-bol 0)
       ((parent-is "diagram_sequence") parent-bol ,offset)
       ((parent-is "^sequence_stmt_loop$") parent-bol ,offset)
       ((parent-is "sequence_stmt_alt_branch") parent-bol ,offset)
       (catch-all parent-bol 0)))))

(defvar mermaid-ts-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-c") #'mermaid-ts-mode-compile-buffer)
    map)
  "Keymap for `mermaid-ts-mode'.")

;;;###autoload
(define-derived-mode mermaid-ts-mode prog-mode "Mermaid"
  "Major mode for editing MermaidJS using tree-sitter."
  (when (treesit-ready-p 'mermaid)
    (treesit-parser-create 'mermaid)

    ;; Comments
    (setq-local comment-start "%% ")
    (setq-local comment-start-skip (rx "%%" (* (syntax whitespace))))

    ;; Electric
    (setq-local electric-indent-chars
                (append "{}:" electric-indent-chars))

    ;; Syntax Highlighting
    (setq-local treesit-font-lock-settings mermaid-ts-mode--treesit-settings)
    (setq-local treesit-font-lock-feature-list '((comment string)
                                                 (keyword builtin type constant variable)
                                                 (escape-sequence)))

    ;; Indent
    (setq-local treesit-simple-indent-rules mermaid-ts-mode--treesit-indent-rules)

    (treesit-major-mode-setup)

    :syntax-table mermaid-ts-mode-syntax-table))

;; Org Babel

(defvar org-babel-default-header-args:mermaid
  '((:results . "file") (:exports . "results"))
  "Default arguments for evaluating a mermaid source block.")

(defun org-babel-execute:mermaid (body params)
  "Compile Mermaid code in an Org mode source block."
  (let* ((out-file (or (cdr (assoc :file params))
                       (error "Mermaid requires a \":file\" header argument")))
         (temp-file (org-babel-temp-file "mermaid-"))
         (cmd (concat (shell-quote-argument mermaid-ts-mode-mmdc-command)
                      " -o " (org-babel-process-file-name out-file)
                      " -i " temp-file)))
    (with-temp-file temp-file (insert body))
    (org-babel-eval cmd "")
    nil))

(provide 'mermaid-ts-mode)
;;; mermaid-ts-mode.el ends here.
